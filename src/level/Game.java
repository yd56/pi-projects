package level;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import character.InGameEntity;
import character.Player;
import character.TrashCan;
import painting.Frame;
import painting.Painter;

public class Game {
	public static Random generator = new Random();
	private Level level;
	public Game() {
		level = new Level();
		level.createLevel();
		//start game
		game();
	}

	// fuer getrennte update und draw timer schneller scheduelen und update nicht immer ausfuehren
	private void game() {
		Timer timer = new Timer();
		TimerTask tick = new TimerTask() {
			@Override
			public void run() {

			update();
			draw();
			}
		};
		timer.schedule(tick, 0, 1000/60);
	}

	private void update() {
		for(Iterator<InGameEntity> it = level.getEntities(); it.hasNext();) {
			it.next().Update();
		}
	}
	
	//in case game actually gets bigger and therefore race conditions become a problem, use Thread.setPriority to give update some breathing room
	private void draw() {
		for(Iterator<InGameEntity> it = level.getEntities(); it.hasNext();) {
			InGameEntity entity = it.next();
			entity.draw();
		}
		level.getScreen().draw();
	}
}