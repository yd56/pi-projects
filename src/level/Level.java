package level;

import java.awt.Image;
import java.awt.Rectangle;

import character.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.imageio.ImageIO;

import character.InGameEntity;
import character.Player;
import character.TrashCan;
import painting.Frame;
import painting.Painter;

public class Level {
	private List<Image> panels; 
	private List<InGameEntity> entities;
	private Painter screen;
	private int height, width;
	public void createLevel() {
		entities = new CopyOnWriteArrayList<InGameEntity>();
		screen = new Painter();
		Frame screenHolder = new Frame(screen);
		Player player = new Player(screenHolder.getKeyboard(), screen, this);
		entities.add(new TrashCan(1000, 500, screen, this));
		entities.add(player);
		height = 800;
		width = 1500;
		panels = new ArrayList<Image>();
				Image panel1;
				try {
					panel1 = ImageIO.read(Level.class.getResourceAsStream("panel1.png"));
				for(int i = 0; i < 10; i++) {
					panels.add(panel1);					
				}		
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	
	public Painter getScreen() {
		return screen;
	}
	
	public Iterator<InGameEntity> getEntities(){
		//remove old entries before handing out the iterator, current implementation does not allow for removal in iterator
		for(int i = 0; i < entities.size(); i ++) {
			if(entities.get(i).isDead() == true) {
				entities.remove(i);
			}
		}
		return entities.iterator();
	}
	public boolean projectileMove(Vector<Integer> vec, Projectile prc) {
		if(!isInLevelBox(prc, vec.get(0), vec.get(1))) {
			return false;
		}
		Iterator<InGameEntity> it = entities.iterator();
		while(it.hasNext()) {
			InGameEntity entityHit = it.next();
			if(!(entityHit instanceof Player) && !(entityHit instanceof Projectile)) {
				if(entityHit.getX() != prc.getX() || prc.getY() != entityHit.getY()) {
					if(collisionDetect(prc, entityHit, vec.get(0), vec.get(1)) == true) {
						return false;
					}
				}				
			}
			}
		return true;
	}
	
	public boolean requestMove(Vector<Integer> vec, InGameEntity entity) {
		if(entity instanceof Particle) {
			return true;
		}
		if(!isInLevelBox(entity, vec.get(0), vec.get(1))) {
			return false;
		}
		Iterator<InGameEntity> it = entities.iterator();
		while(it.hasNext()) {
			InGameEntity entityHit = it.next();
			if(entityHit.getX() != entity.getX() || entity.getY() != entityHit.getY()) {
				if(collisionDetect(entity, entityHit, vec.get(0), vec.get(1)) == true) {
					return false;
				}
			}
		}
		return true;
		}
	/**
	 * detects collision of o1 + xy and o2
	 * @param o1
	 * @param o2
	 * @param x1
	 * @param y2
	 * @return
	 */
	private boolean collisionDetect(InGameEntity o1, InGameEntity o2, int x1, int y1) {//x,y = versatz zur aktuellen 
		if(!o1.hasHitbox() || !o2.hasHitbox()) {
			return false;
		}
		Rectangle rect1 = new Rectangle(o1.getX() + x1, o1.getY() + o1.getHeight() - o1.getDepth() + y1, o1.getWidth(), o1.getDepth());
		Rectangle rect2 = new Rectangle(o2.getX(), o2.getY() + o2.getHeight() - o2.getDepth(), o2.getWidth(), o2.getDepth());
		if(rect1.intersects(rect2)) {
			if(o2 instanceof  PhysicEntity) {
				double degree = Math.acos(x1/(Math.sqrt(x1*x1 + y1*y1)));
				if(y1 > 0) {
					degree = Math.acos(-x1/(Math.sqrt(x1*x1 + y1*y1))) + Math.PI;
				}
					((PhysicEntity) o2).accelerate(degree);//if collision detected, push the physicsObject away
					}
			return true;
		}
		return false;
		}
	private boolean isInLevelBox(InGameEntity o, int x, int y) {
		
		if(o.getX() + x < 0 || o.getY() + y < 0) {
			if(o instanceof Projectile) {
				o.killMe();
			}
			return false;
		}
		if(o.getX() + x  + o.getWidth() > width || o.getY() + y + o.getHeight() > height) {
			if(o instanceof Projectile) {
				o.killMe();
			}return false;
		}
		return true;
	}
	
	public boolean spawnRequest(InGameEntity e) {
		entities.add(e);
		return true;
	}
}
