package painting;

import input.Keyboard;
import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import java.awt.event.KeyListener;

public class Frame{
	
	private Painter screen;
	private Keyboard keyboard;
	private JFrame f;
	
	/**
	 * so Logic shares data with it's painter
	 * @param data
	 */
	public Frame(Painter screen) {
		//muss hier gemacht werden, weil keyListener das Jframe braucht
		keyboard = new Keyboard();
        f = new JFrame("Swing Paint Luigi");
		this.screen = screen;
        createAndShowGUI(); 
	}

    private void createAndShowGUI() {
        f.pack();
        f.setSize(1920, 800);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        f.add(screen);
        f.setFocusable(true);
        f.requestFocus();
        f.addKeyListener((KeyListener)keyboard);
    } 
    public Keyboard getKeyboard() {
    	return keyboard;
    }
    public void setTitle(String title) {
    	f.setTitle(title);
    }
}

