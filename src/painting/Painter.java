package painting;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.PriorityBlockingQueue;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import character.InGameEntity;

public class Painter extends JPanel{

	private static final long serialVersionUID = -54279050588172728L;
	private List<Image> backgrounds;
	private PriorityBlockingQueue<InGameEntity> entitys; //hier evtl andere Datenstruktur mit die ge�ndert und nnicht neu bef�llt wird
    public Painter() {
        setBorder(BorderFactory.createLineBorder(Color.black));
        entitys = new PriorityBlockingQueue<InGameEntity>(100, new Comparator<InGameEntity>() {

			@Override
			public int compare(InGameEntity o1, InGameEntity o2) {
			try {
				return o1.getY() + o1.getHeight() <= o2.getY() + o2.getHeight() ? -1 : 1;				
			} catch(NullPointerException e) {
				System.out.println(o1);
				System.out.println(o2);
				System.out.println("^");
			}
			return 0;
			}
        	
		});
    }
    
    /**
     * @param x
     * @param y
     */
    public void draw() {
    	repaint();
    }
    

    
    //need to do all painting here
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);  
        while(!entitys.isEmpty()) {
        	InGameEntity entity = entitys.poll();
        	g.drawImage(entity.getImage(), (int)entity.getX()  , (int)entity.getY(), null);   
        }

//        
//        if(backGroundPanels != null) {
//        	int i = 0;
//	        for(long x = 0 ; (x < data.getX() + Data.RENDERWIDTH && backGroundPanels.size() > i); i++) {
//	        	Image panel = backGroundPanels.get(i);
//	        	if(panel != null) {
//	        		if(x > (data.getX()) - panel.getWidth(null)) {
//	        			g.drawImage(backGroundPanels.get(i), (int)(x- data.getX()), 0, null);
//	        			}
//	        		x += panel.getWidth(null);        		
//	        		}        
//	        	}
        }
        
        //entitys
//        InGameEntity currentEntitiy;
//        for(Iterator<InGameEntity> entitiyI = data.getAllImages().iterator(); entitiyI.hasNext(); ) {
//        	currentEntitiy = entitiyI.next();
//        	g.drawImage(currentEntitiy.getImage(), (int)(currentEntitiy.getX()- data.getX() + Data.RENDERWIDTH/4)  , (int)currentEntitiy.getY(), null);
//        }
        
//        //paint watch hands
//        InGameEntity player = data.getPlayer();
//        clock = clock + 0.05;
//        Graphics2D g2 = (Graphics2D) g;
//        g2.setStroke(new BasicStroke(4));
//        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
//        g2.drawLine(60 + Data.RENDERWIDTH/4, (int)player.getY() + 120, 60 + Data.RENDERWIDTH/4 + (int)(Math.cos(clock/10) * 20),  ((int)player.getY() + 120 + (int)(20 * Math.sin(clock/10))));
//        g2.drawLine(60 + Data.RENDERWIDTH/4, (int)player.getY() + 120, 60 + Data.RENDERWIDTH/4 + (int)(Math.cos(clock/120) * 16),  ((int)player.getY() + 120 + (int)(16 * Math.sin(clock/120))));
     
    public void addEntity(InGameEntity entity) {
    	if(entity.getImage() != null)
    	entitys.add(entity);
    }

    
}
