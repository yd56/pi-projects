package input;

import java.util.Set;

/**
 * define behavior of all controllers etc here, so the player does not need to care which controller he gets
 * @author Yasin
 *
 */
public abstract class InputClass extends Thread{
	
	public abstract Set<Character> getKeys();
}
