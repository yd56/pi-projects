package input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

public class Keyboard extends InputClass implements KeyListener{
    private Set<Character> pressed;
	public Keyboard() {
		pressed = new ConcurrentSkipListSet<Character>();
	}
	
	
	public void run() {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		pressed.add(e.getKeyChar());
	}

	@Override
	synchronized public void keyReleased(KeyEvent e) {
		pressed.remove(e.getKeyChar());
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		//nothing
	}


	@Override
	public Set<Character> getKeys() {
		// TODO Auto-generated method stub
		return pressed;
	}


}
