package character;

import java.awt.Image;
import java.util.Vector;

import level.Level;
import painting.Painter;

/**
 * not used atm, use when doing velocity and/or collision
 * @author Yasin
 *
 */
public abstract class PhysicEntity extends InGameEntity{
	protected int MAX_VELOCITY;
	protected int weight;
	protected int velocityX;
	protected int velocityY;
	protected int velPerTick;
	public PhysicEntity(int i, int j, Painter screen, Level level) {
		super(i, j, screen, level);
	}
	public PhysicEntity() {
		
	}
	public int getVelocity() {
		return velocityX;
	}
	public void accelerate(double degree) {
		velocityX += Math.cos(degree) * 20;
		velocityY -= Math.sin(degree) * 20;
		System.out.println(velocityX);
	};
	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	/**
	 * slows down velocity
	 */
	public void Update() {
		// TODO Auto-generated method stub
		Vector<Integer> vec = new Vector<Integer>();
		vec.addElement(velocityX);
		vec.addElement(velocityY);
		moveVec(vec);
		if(velocityX > 0)
			velocityX --;
		if(velocityY > 0)
			velocityY --;
		if(velocityX < 0)
			velocityX ++;
		if(velocityY < 0)
			velocityY ++;
	}
	
	
}
