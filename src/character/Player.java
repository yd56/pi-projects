package character;

import java.awt.Image;
import java.io.IOException;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.imageio.ImageIO;
import input.InputClass;
import level.Level;

public class Player extends InGameEntity {
	private int jumpTimer1 = 0;
	private int jumpTimer2 = 0;
	public boolean moving = false;
	private int moved = 0;
	private Image standing;
	private Image moving1;
	private Image moving2;;
	private InputClass input;
	private int idleTicker;
	private boolean bulletAvailible;
	private int bulletTick;
	private double degreeOfLastMove;
	
	public Player(InputClass input, painting.Painter screen, Level level) {
		super(128, 292, screen, level);
		try {
			standing = ImageIO.read(getClass().getResourceAsStream("dude.png")).getScaledInstance(128, 292, 0);
			moving1 = ImageIO.read(getClass().getResourceAsStream("dudeMove1.png")).getScaledInstance(128, 292, 0);
			moving2 = ImageIO.read(getClass().getResourceAsStream("dudeMove2.png")).getScaledInstance(128, 292, 0);
			image = standing;
			depth = image.getHeight(null)/9;
		} catch (IOException e) {
		}
		this.input = input;
	}
	
	public void jump(int hight) {
		if(jumpTimer1 == 0 && jumpTimer2 == 0) {
			Timer tickTimer = new Timer();
			TimerTask tick = new TimerTask() {

				public void run() {
//					moveY(-hight+jumpTimer1);
					jumpTimer1 ++;
					if(jumpTimer1 == 10) {
						this.cancel();
						jumpTimer1 = 0;
						Timer tickTimer2 = new Timer();
						TimerTask tick2 = new TimerTask() {
							public void run() {

//								moveY(hight-jumpTimer2);
								jumpTimer2 ++;
								if(jumpTimer2  == 10) {
									this.cancel();
									jumpTimer2 = 0;
								}
							}
						};
						tickTimer2.schedule(tick2, (long)0, 1000/60);
					}
				}	
			};
			tickTimer.schedule(tick, (long)0, 1000/60);
		}
	}
	
	
	
	public void idle() {
		moving = false;
	}
	
	public Image getImage() {
		if(!moving) {
			image = standing;
		} else if(moved %40 < 20) {
			image = moving1;
		} else {
			image = moving2;
		}
		return image;
	}

	@Override
	public void Update() {
		//fetch input
		Set<Character> keys = input.getKeys();
		//do Logic based on input
		boolean idleFlag = true;
		moving = false;
		int x = 0,y = 0;
		if(keys.contains('d') && !keys.contains('a')) {
			x=10;
		}		
		if(keys.contains('a') && !keys.contains('d')) {
			x = -10;
		}		
		if(keys.contains('w') && !keys.contains('s')) {
			y = -10;
		}		
		if(keys.contains('s') && !keys.contains('w')) {
			y = 10;
		}
		if(keys.contains('p')) {
			if(bulletAvailible) {
				level.spawnRequest(new Projectile(getX(), getY(), screen, level, degreeOfLastMove));
				bulletAvailible = false;
			}
		}
		
		//check if movement has been done
		if(x != 0 || y != 0) {
			moving = true;
			Vector<Integer> move = new Vector<Integer>();
			move.addElement(x);
			move.addElement(y);
//			idleTicker = 0;
			idleFlag = !moveVec(move);
			//maybe put stuff like this in utils
			degreeOfLastMove = Math.acos(x/(Math.sqrt(x*x + y*y)));
			if(y > 0) {
				degreeOfLastMove = Math.acos(-x/(Math.sqrt(x*x + y*y))) + Math.PI;
			}
		}
		if(idleFlag){
			if(idleTicker < 8)
				idleTicker ++;
			if(idleTicker > 7)
				idle();
		} else {
			moved ++;
		}
		
		//load
		if(!bulletAvailible) {
			bulletTick++;
			if(bulletTick == 60) {
				bulletAvailible = true;
				bulletTick = 0;
			}
		}
		
	}
}

	
