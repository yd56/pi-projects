package character;

import java.awt.Image;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;

import level.Level;
import painting.Painter;

public class Particle extends InGameEntity {

	private int lifetime;
	private double degree;
	private int speed;
	private int alive = 0;
	private boolean up;
	public Particle(int width, int height, int x, int y, Painter screen, Level level, int lifetime, double degree, int speed, Image image, boolean up) {
		super(width, height, screen, level);
		setX(x);
		setY(y);
		collision = false;
		this.degree = degree;
		this.lifetime = lifetime;
		this.speed = speed;
		this.image = image;
		this.up = up;
	}


	@Override
	public Image getImage() {
		return image;
	}

	@Override
	public void Update() {
		// TODO Auto-generated method stub
		Vector<Integer> move = new Vector<Integer>();
		move.addElement((int) (Math.cos(degree) * speed));
		int gravity = 0;
		if(up) {
			gravity = - 2;
		}
		move.addElement((int) -(Math.sin(degree) * speed) + gravity);
		moveVec(move);
		lifetime --;
		alive ++;
		if(speed > 5) {
			speed --;
		}
		if(lifetime <= 0) {
			killMe();
		}
	}

}
