package character;

import java.awt.Image;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;

import level.Level;
import painting.Painter;

public class TrashCan extends PhysicEntity {

	public TrashCan(int x, int y, Painter screen, Level level) {
		super(38*4, 46*4, screen, level);
		try {
			image = ImageIO.read(getClass().getResourceAsStream("trashCan.png"));
			image = image.getScaledInstance(4 * image.getWidth(null), 4 * image.getHeight(null), 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		vec = new Vector<Integer>();
		vec.setSize(2);
		vec.set(0, x);
		vec.set(1, y);
		hp = 10;
	}
	@Override
	public Image getImage() {
		return image;
	}
	@Override
	public void Update() {
		super.Update();
	}

}
