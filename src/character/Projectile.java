package character;

import java.awt.Image;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;

import level.Game;
import level.Level;
import painting.Painter;

public class Projectile extends InGameEntity{
	final int SPEED = 20;
	final double degree;
	public Projectile(int x, int y, Painter screen, Level level, double degree) {
		super(128, 128, screen, level);
		this.degree = degree;
		setX(x);
		setY(y);
		try {
			image = ImageIO.read(getClass().getResourceAsStream("hadoken.png")).getScaledInstance(128, 128, 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Image getImage() {
		return image;
	}

	@Override
	public void Update() {
		Vector<Integer> move = new Vector<Integer>();
		move.addElement((int) (Math.cos(degree) * SPEED));
		move.addElement((int) -(Math.sin(degree) * SPEED));
		moveVec(move);
	}
	
	@Override
	public boolean moveVec(Vector<Integer> vecMove) {
		if(level.projectileMove(vecMove, this)) {
			vec.set(0,vec.get(0) + vecMove.get(0));
			vec.set(1,vec.get(1) + vecMove.get(1));
			return true;
		} else {
			explode();
		}
		return false;	
	}
	
	
	public void explode() {
		//do stuff then die
		//dont do io here! in startup sth
		Image[] fire = new Image[8];
		try {
			for(int j = 1; j < 8; j++) {
				fire[j] = ImageIO.read(getClass().getResourceAsStream("smokeParticle" + j + ".png"));
				fire[j] = fire[j].getScaledInstance( fire[j].getWidth(null) * 2, fire[j].getHeight(null) *2, 0);
			}
		} catch (IOException e) {
		}
		for(int j = 0; j < 8; j++) {
			for(int i = 0; i < 20; i++) {
				level.spawnRequest(new Particle(4, 4, 
						getX() + Game.generator.nextInt(width), getY() + Game.generator.nextInt(height), screen, level,
						12 + Game.generator.nextInt(30), (Game.generator.nextDouble() *2 -1) * 2 * Math.PI, 2 + Game.generator.nextInt(j+3), fire[j], false));			
			}		
		}

		killMe();
	}

}
