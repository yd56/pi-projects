package character;

import java.awt.Image;
import java.util.Vector;
import level.Level;
import painting.Painter;

public abstract class InGameEntity {
	protected Vector<Integer> vec;
	protected int height;
	protected int width;
	protected Image image;
	protected Painter screen;
	private boolean deadFlag;
	protected int depth;
	protected Level level;
	protected boolean collision = true;
	protected int hp;
	
	/**
	 * unsure if this should be used, or if all entities should just have their own set subclass constructor
	 * use this for now, more flexible i guess
	 * -> make factory for standards
	 * 
	 * ^deprecated thought
	 * @param width
	 * @param 
	 */
	public InGameEntity(int width, int height, Painter screen, Level level) {
		this.height = height;
		this.width = width;
		vec = new Vector<Integer>();
		vec.addElement(0);
		vec.addElement(0);
		this.screen = screen;
		depth = height/8;
		this.level = level;
	}
	public Vector<Integer> returnVec() {
		return vec;
	}
	public InGameEntity() {
		
	}
	
	/**
	 * returns false for display only items
	 * @return
	 */
	public boolean hasHitbox() {
		return collision;
	}
	public int getDepth() {
		return depth;
	}
	public int getX() {
		return vec.get(0);
	}

	public void setX(int x) {
		vec.set(0,x);
	}
	
	public boolean moveVec(Vector<Integer> vecMove) {
		if(level.requestMove(vecMove, this)) {
			vec.set(0,vec.get(0) + vecMove.get(0));
			vec.set(1,vec.get(1) + vecMove.get(1));
			return true;
		}
		return false;
	}
	
	public void draw() {
		sendDrawRequest();
	}

	public int getY() {
		return (vec.get(1) == null) ? 0 : vec.get(1);
	}

	public void setY(int y) {
		vec.set(1,y);
	}

	public abstract Image getImage();
	
	public abstract void Update();
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	} 
	protected void sendDrawRequest() {
		screen.addEntity(this);
	}
	public boolean isDead() {
		return deadFlag;
	}
	public void killMe() {
		deadFlag = true;
	}
}
